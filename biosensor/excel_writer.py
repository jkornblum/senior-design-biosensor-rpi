'''
Created on Feb 7, 2014

@author: Josh Kornblum
'''

class writer(object):
    '''
    Excel Writer 
    '''


    def __init__(self, openpyxl_workbook):
        '''
        Constructor
        '''
        
        self.workbook = openpyxl_workbook
        
    def save(self, filepath):
        '''
        Saves Excel file workbook, use \\ double-backslashes for Windows save and / for Unix saves
        '''
        self.workbook.save(filepath);