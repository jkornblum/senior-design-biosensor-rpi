'''
Created on Feb 7, 2014

@author: Josh Kornblum
'''

import numpy
import openpyxl as pyxl

_RAW_BLOCK_SIZE = 6404096 #Number of bytes for raw data of full resolution frame (2592x1944)
_HEADER_SIZE = 32768 #Header on raw data size
_ROW_SIZE = 3264 #Number of bytes in horizontal row
_H_PIXELS = 2592 #Number of horizontal pixels on OV5647 sensor
_V_PIXELS = 1944 #Number of vertical pixels on OV5647 sensor
_TRAILER_LENGTH = 26112 #Number of bytes after pixel information, purpose unknown

class analyzer(object):
    '''
    classdocs
    '''
    
    #The information below is taken from Raspberry Pi website and is the foundation for the frame anaylze algorithm - jmk
    
    # When you generate a "jpeg with raw" file using the '--raw' option to raspistill, 
    #the raw data is literally tacked onto the end of a standard JPEG file. This raw data block is always 6404096 bytes in size.
    # The JPEG data varies in size with the image, but raw data block size is fixed, so you can obtain just the raw data by extracting the last 
    #6404096 bytes of the file.
    # 
    # Of that raw data block, the first 32768 bytes is header data, starting with the string "BRCM" and ending with a lot of 0x00 bytes. 
    # 
    # After the header is 6371328 bytes of (mostly) image pixel data, where each set of 5 bytes encodes four 10-bit pixels.
    # The first 4 bytes are the high-order bits of 4 pixels, and the 5th byte contains the low-order two bits of each of the four pixels,
    # packed together. The pixels have a Bayer pattern, so a de-Bayer process must be run to obtain R-G-B data. I haven't done the decode step yet, 
    #but there are only a few standard Bayer patterns.
    # 
    # The pixel data is arranged in the file in rows, with 3264 bytes per row. With 2592 image pixels across on the sensor, 
    #and 5/4 bytes per pixel, you might expect only 3240 bytes per row, but there are 24 extra bytes at the end of each row which encode some other,
    # unknown information. Some of that may be masked-off ("dark") edge pixels used for calibrating the black level. (EDIT: I previously claimed 28 extra 
    #bytes per row but the above numbers now believed correct.)
    # Now, with 3264 bytes per row x 1944 rows we have 6345216 bytes, that is the full 2592x1944 image area. 
    #The file data continues with 26112 additional bytes after that, the purpose of which I do not know.


    def __init__(self, filepath):
        '''
        Constructor
        '''
             
        #File readers       
        try:
            self._file = numpy.fromfile(filepath, dtype = numpy.uint8)
            self._file_size = self._file.size
        
            self._raw_file = self._file[(self._file_size - _RAW_BLOCK_SIZE) + _HEADER_SIZE:self._file_size - _TRAILER_LENGTH]
        
            if (self._file_size < _RAW_BLOCK_SIZE):
                raise Exception("File not long enough to contain RAW data")
        except:
            raise Exception("Could not open file correctly")
        
        #Create excel workbook
        self.workbook = pyxl.Workbook()
               
    def analyze_all(self):
        '''
        Creates self.<color>_v_h arrays
        Creates self.workbook with all color sheets
        '''
        
        #Create worksheets
        ws_b = self.workbook.active
        ws_b.title = "Sensor Blue Data"
        ws_g1 = self.workbook.create_sheet(1, "Sensor Green 1 Data")
        ws_g2 = self.workbook.create_sheet(2, "Sensor Green 2 Data")
        ws_r = self.workbook.create_sheet(3, "Sensor Red Data")
        
        #Create v_h arrays
        self.blue_v_h = []
        self.green1_v_h = []
        self.green2_v_h = []
        self.red_v_h = []

        byte_counter = 0
        
        for v_pixel in range(0, _V_PIXELS): #VPIXELS = Horizontal Rows
            row_buffer = self._raw_file[byte_counter: byte_counter + 3240]
            byte_counter += 3264
            h_pixel = 0
            
            red_h = []
            green1_h = []
            green2_h = []
            blue_h = []
            
            for h_pos in range(0, 3240, 5):
                h_pos_b = h_pos
                h_pos_g1 = h_pos + 1
                h_pos_g2 = h_pos + 2
                h_pos_r = h_pos + 3
                h_pos_combined = h_pos + 4
                
                blue_h.append((row_buffer[h_pos_b] << 2) + (row_buffer[h_pos_combined] & 0b11000000))
                green1_h.append((row_buffer[h_pos_g1] << 2) + ((row_buffer[h_pos_combined] & 0b00110000) << 2))
                green2_h.append((row_buffer[h_pos_g2] << 2) + ((row_buffer[h_pos_combined] & 0b00001100) << 4))
                red_h.append((row_buffer[h_pos_r] << 2) + ((row_buffer[h_pos_combined] & 0b00000011) << 6))
                
                ws_b.cell(row=v_pixel, column=h_pixel).value = blue_h[h_pixel]
                ws_g1.cell(row=v_pixel, column=h_pixel).value = green1_h[h_pixel]
                ws_g2.cell(row=v_pixel, column=h_pixel).value = green2_h[h_pixel]
                ws_r.cell(row=v_pixel, column=h_pixel).value = red_h[h_pixel]
                
                h_pixel += 1
             
            print v_pixel                               
            self.blue_v_h.append(blue_h)
            self.green1_v_h.append(green1_h)
            self.green2_v_h.append(green2_h)
            self.red_v_h.append(red_h)

    def analyze_blue(self):
        '''
        Creates self.blue_v_h array
        Creates self.workbook blue worksheet
        '''
        
        #Create worksheets
        ws_b = self.workbook.active
        ws_b.title = "Sensor Blue Data"
        
        #Create v_h arrays
        self.blue_v_h = []

        byte_counter = 0
        
        for v_pixel in range(0, _V_PIXELS): #VPIXELS = Horizontal Rows
            row_buffer = self._raw_file[byte_counter: byte_counter + 3240]
            byte_counter += 3264
            h_pixel = 0

            blue_h = []
            
            for h_pos in range(0, 3240, 5):
                h_pos_b = h_pos
                h_pos_combined = h_pos + 4
                
                blue_h.append((row_buffer[h_pos_b] << 2) + (row_buffer[h_pos_combined] & 0b11000000))
                
                ws_b.cell(row=v_pixel, column=h_pixel).value = self.blue_h[h_pixel]
                
                h_pixel += 1
                                            
            self.blue_v_h.append(blue_h)
    
    def analyze_green(self):
        '''
        Creates self.<green 1/2>_v_h arrays
        Creates self.workbook Green Sheets
        '''
        
        #Create worksheets
        ws_g1 = self.workbook.active
        ws_g1.title = "Sensor Green 1 Data"
        ws_g2 = self.workbook.create_sheet(2, "Sensor Green 2 Data")
        
        #Create v_h arrays
        self.green1_v_h = []
        self.green2_v_h = []

        byte_counter = 0
        
        for v_pixel in range(0, _V_PIXELS): #VPIXELS = Horizontal Rows
            row_buffer = self._raw_file[byte_counter: byte_counter + 3240]
            byte_counter += 3264
            h_pixel = 0
            
            green1_h = []
            green2_h = []
            
            for h_pos in range(0, 3240, 5):
                h_pos_g1 = h_pos + 1
                h_pos_g2 = h_pos + 2
                h_pos_combined = h_pos + 4
                
                green1_h.append((row_buffer[h_pos_g1] << 2) + ((row_buffer[h_pos_combined] & 0b00110000) << 2))
                green2_h.append((row_buffer[h_pos_g2] << 2) + ((row_buffer[h_pos_combined] & 0b00001100) << 4))
                
                ws_g1.cell(row=v_pixel, column=h_pixel).value = green1_h[h_pixel]
                ws_g2.cell(row=v_pixel, column=h_pixel).value = green2_h[h_pixel]
                
                h_pixel += 1
                                            
            self.green1_v_h.append(green1_h)
            self.green2_v_h.append(green2_h)

    def analyze_red(self):
        '''
        Creates self.<color>_v_h arrays
        Creates self.workbook
        '''
        
        #Create worksheets
        ws_r = self.workbook.active
        ws_r.title = "Sensor Red Data"
        
        #Create v_h arrays
        self.red_v_h = []

        byte_counter = 0
        
        for v_pixel in range(0, _V_PIXELS): #VPIXELS = Horizontal Rows
            row_buffer = self._raw_file[byte_counter: byte_counter + 3240]
            byte_counter += 3264
            h_pixel = 0
            
            red_h = []
            
            for h_pos in range(0, 3240, 5):
                h_pos_r = h_pos + 3
                h_pos_combined = h_pos + 4

                red_h.append((row_buffer[h_pos_r] << 2) + ((row_buffer[h_pos_combined] & 0b00000011) << 6))

                ws_r.cell(row=v_pixel, column=h_pixel).value = red_h[h_pixel]
                
                h_pixel += 1
               
            #print v_pixel
            self.red_v_h.append(red_h)            