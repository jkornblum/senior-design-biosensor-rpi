'''
Created on Feb 2, 2014

@author: Josh
'''

import biosensor
import os, numpy
import matplotlib.pyplot as plt
from openpyxl import Workbook

RAWBLOCKSIZE = 6404096
HEADERSIZE = 32768
ROWSIZE = 3264
HPIXELS = 2592 #number of horizontal pixels on OV5647 sensor
VPIXELS = 1944 #number of vertical pixels on OV5647 sensor
TRAILERLENGTH = 26112 #Number of bytes after pixel information, purpose unknown

#[][] -- [V][H]
red_h = []
green1_h = []
green2_h = []
blue_h = []

blue_v_h = []
green1_v_h = []
green2_v_h = []
red_v_h = []

wb = Workbook()
ws = wb.active
ws.title = "Sensor Data Red"

ir = biosensor.image_reader()
file_len = ir.get_img_size("C:\\Users\\Josh\\Desktop\\image2.jpg")
print file_len

if (file_len < RAWBLOCKSIZE):
    raise Exception("File not long enough to contain RAW data")

imgf = open("C:\\Users\\Josh\\Desktop\\image2.jpg")
# for one file, (TotalFileLength:11112983 - RawBlockSize:6404096) + Header:32768 = 4741655
# The pixel data is arranged in the file in rows, with 3264 bytes per row.
# with 3264 bytes per row x 1944 rows we have 6345216 bytes, that is the full 2592x1944 image area.

#offset = (file_len - RAWBLOCKSIZE) + HEADERSIZE; # location in file the raw pixel data starts
#imgf.seek(offset, os.SEEK_SET)

# When you generate a "jpeg with raw" file using the '--raw' option to raspistill, 
#the raw data is literally tacked onto the end of a standard JPEG file. This raw data block is always 6404096 bytes in size.
# The JPEG data varies in size with the image, but raw data block size is fixed, so you can obtain just the raw data by extracting the last 
#6404096 bytes of the file.
# 
# Of that raw data block, the first 32768 bytes is header data, starting with the string "BRCM" and ending with a lot of 0x00 bytes. 
# 
# After the header is 6371328 bytes of (mostly) image pixel data, where each set of 5 bytes encodes four 10-bit pixels.
# The first 4 bytes are the high-order bits of 4 pixels, and the 5th byte contains the low-order two bits of each of the four pixels,
# packed together. The pixels have a Bayer pattern, so a de-Bayer process must be run to obtain R-G-B data. I haven't done the decode step yet, 
#but there are only a few standard Bayer patterns.
# 
# The pixel data is arranged in the file in rows, with 3264 bytes per row. With 2592 image pixels across on the sensor, 
#and 5/4 bytes per pixel, you might expect only 3240 bytes per row, but there are 24 extra bytes at the end of each row which encode some other,
# unknown information. Some of that may be masked-off ("dark") edge pixels used for calibrating the black level. (EDIT: I previously claimed 28 extra 
#bytes per row but the above numbers now believed correct.)
# Now, with 3264 bytes per row x 1944 rows we have 6345216 bytes, that is the full 2592x1944 image area. 
#The file data continues with 26112 additional bytes after that, the purpose of which I do not know.

imgb = numpy.fromfile("C:\\Users\\Josh\\Desktop\\image2.jpg", dtype = numpy.uint8)
print imgb.size;
img_numpy_data = imgb[(file_len - RAWBLOCKSIZE) + HEADERSIZE:imgb.size - TRAILERLENGTH]
print img_numpy_data.size; #3264 bytes per row x 1944 rows we have 6345216 bytes, that is the full 2592x1944 image area

byte_counter = 0
h_pixel = 0

for v_pixel in range(0, VPIXELS): #VPIXELS = Horizontal Rows
    row_buffer = img_numpy_data[byte_counter:byte_counter+3240]
    byte_counter += 3264
    h_pixel = 0
    
    red_h = []
    green1_h = []
    green2_h = []
    blue_h = []
    
    for h_pos in range(0, 3240, 5):
        #h_pos_b = h_pos
        #h_pos_g1 = h_pos + 1
        #h_pos_g2 = h_pos + 2
        h_pos_r = h_pos + 3
        h_pos_combined = h_pos + 4
        
        #blue_h.append((row_buffer[h_pos_b] << 2) + (row_buffer[h_pos_combined] & 0b11000000))
        #green1_h.append((row_buffer[h_pos_g1] << 2) + ((row_buffer[h_pos_combined] & 0b00110000) << 2))
        #green2_h.append((row_buffer[h_pos_g2] << 2) + ((row_buffer[h_pos_combined] & 0b00001100) << 4))
        red_h.append((row_buffer[h_pos_r] << 2) + ((row_buffer[h_pos_combined] & 0b00000011) << 6))
        
        h_pixel += 1
        
    
    #print "Byte Number: " + str(byte_counter)
    #print "V Pixel Number: " + str(v_pixel)
    #blue_v_h.append(blue_h)
    #green1_v_h.append(green1_h)
    #green2_v_h.append(green2_h)
    red_v_h.append(red_h)
    
for v_pixel in range(0, VPIXELS):
    for h_pixel in range(0,648):
        ws.cell(row=v_pixel, column=h_pixel).value = red_v_h[v_pixel][h_pixel]

wb.save("C:\\Users\\Josh\Desktop\\test-sd-output.xlsx")
    
# plt.scatter(range(0,648), red_v_h[1000])
# for x in range(0, 648):
#     print str(red_v_h[1000][x]) + " : " + str(x)
# plt.show()
        
