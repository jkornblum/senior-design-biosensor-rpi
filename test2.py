'''
Created on Feb 7, 2014

@author: Josh
'''

from biosensor import frame_analyzer, excel_writer

#Analyze red pixels only
fa = frame_analyzer.analyzer("C:\\Users\\Josh\\Desktop\\image2.jpg")
print "Analyzing"
fa.analyze_all()


xlw = excel_writer.writer(fa.workbook)
print "Saving"
xlw.save("C:\\Users\\Josh\\Desktop\\biosensor-output.xlsx")
print "Complete"